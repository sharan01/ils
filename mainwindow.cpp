#include "mainwindow.h"
#include "issuebook.h"

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent)
{
    qDebug() << "main window constructed";

    createResources();
    createWidgets();
    createLayout();
}
void MainWindow::createResources()
{
    bIcon.addFile(":/res/images/book.png");
    sIcon.addFile(":/res/images/student.png");
    aIcon.addFile(":/res/images/admin.png");
    siz.setWidth(20);
    siz.setHeight(20);
}

void MainWindow::createWidgets()
{

    books = new Books(this);
    members = new Members(this);
    admin = new Admin(this);

    setWindowTitle("Aurora's Integrated Library System");


    tabWidget.addTab(books,bIcon,"books  ");
    tabWidget.addTab(members,sIcon,"Students  ");
    tabWidget.addTab(admin,aIcon,"Admin  ");

    tabWidget.setTabPosition(QTabWidget::West);
    tabWidget.setIconSize(siz);

}
void MainWindow::createLayout()
{
    mainLayout.addWidget(&tabWidget);
    //mainWidget.setLayout(&mainLayout);
    //mainWidget.setMinimumSize(700,500);
    setLayout(&mainLayout);

}
void MainWindow::createConnections()
{

}
