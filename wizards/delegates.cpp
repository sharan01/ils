#include "delegates.h"


delegates::delegates(QObject *parent) :
    QStyledItemDelegate(parent)
{
}


 QString delegates::displayText(const QVariant & value, const QLocale & locale ) const
 {
  Q_UNUSED(locale);

     QDate date = QDate::fromJulianDay(value.toInt());
     QString dateInStringFormat = date.toString();

     return dateInStringFormat;
 }

