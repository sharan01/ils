#ifndef DELEGATES_H
#define DELEGATES_H

#include <QStyledItemDelegate>
#include <QDate>

class delegates : public QStyledItemDelegate
{
public:
    delegates(QObject *parent);

    virtual QString displayText(const QVariant & value, const QLocale & locale ) const;

    //mutable QDate date;
};

#endif // DELEGATES_H

